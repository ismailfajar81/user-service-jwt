# user-service-jwt

Spring Boot security using JWT (Json Web Token)


- This repo is based on tutorial video on You Tube provided by [GetArrays Channel](https://www.youtube.com/watch?v=mYKf4pufQWA)

- The application implements the authentication and the authorization based on roles that given the access to some feature such as read and create users role

- It also implements the refresh token that gives permission to the user to be logged in a certain time that has been set 

- This repo uses MariaDB as Database

## Getting Started

- Use `mvn clean install` in the project root directory to build the project.
- Run the main class `UserServiceApplication` to start


## Postman Collection

![Login Page](img/login page.jpg "Login Page")

![users Page](img/users page.jpg "Users Page")

![Refresh Token Page](img/refresh token.jpg "Refresh Token page")
